# README #

### Repository holding Animana asignment code ###

* Tool for calculating T-shirt unit prices with linear interpolation
* 0.0.1

### How do I get set up? ###

* Download source code and go to project home directory
* (Must have **Maven** and **Java 1.8**)
* Build app with **mvn clean install** command
* **animana.jar** is created in target folder
* Run application with **java -jar target/animana.jar**
* You will see instructions for using the tool

### Who do I talk to? ###

* Jovan Kricka (<jovankricka@gmail.com>)