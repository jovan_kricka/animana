package com.animana.exception;

import com.animana.services.calc.PriceCalculationService;

/**
 * Thrown when quantity provided to the {@link PriceCalculationService}
 * is either null or negative integer.
 */
public class InvalidQuantityException extends Exception {

    public InvalidQuantityException() {
        super("Quantity provided to the PriceCalculationService is either null or negative number.");
    }

}
