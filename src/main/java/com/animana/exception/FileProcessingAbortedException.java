package com.animana.exception;

/**
 * Thrown when file processing is aborted due to invalid data.
 */
public class FileProcessingAbortedException extends RuntimeException
{

    public FileProcessingAbortedException()
    {
        super("File processing aborted.");
    }

}
