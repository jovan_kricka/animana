package com.animana.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Application context for Animana application.
 */
@Configuration
@ComponentScan("com.animana")
public class AppContext {
}