package com.animana.app;

import com.animana.cmd.CommandLineHandler;
import com.animana.cmd.CommandLineParameters;
import com.animana.exception.FileProcessingAbortedException;
import com.animana.services.io.FileProcessingService;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * Applications main entry point class.
 */
public class App
{

    private final static Logger logger = Logger.getLogger(App.class);

    public static final String INPUT_FILE_ERROR = "There was an error reading input file.";

    private static ApplicationContext applicationContext;

    public static void main(String[] args)
    {
        initializeSpringContext();

        FileProcessingService fileProcessingService = getFileProcessingService();
        CommandLineHandler commandLineHandler = getCommandLineHandler();

        try
        {
            CommandLineParameters parameters = commandLineHandler.parseCommandLine(args);
            fileProcessingService.processFile(parameters.getFilePath(), parameters.isContinueMode());
        } catch (IOException e)
        {
            logger.error(e, e.getCause());
            System.out.println(INPUT_FILE_ERROR);
        } catch (ParseException e)
        {
            logger.error(e, e.getCause());
            System.out.println(e.getMessage());
        } catch (FileProcessingAbortedException e)
        {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Loads Spring context and initializes beans.
     */
    private static void initializeSpringContext()
    {
        applicationContext = new AnnotationConfigApplicationContext(
                AppContext.class);
    }

    /**
     * Gets File Processing service.
     *
     * @return {@link FileProcessingService}
     */
    private static FileProcessingService getFileProcessingService()
    {
        return applicationContext.getBean(FileProcessingService.class);
    }

    /**
     * Gets Command Line Handler component.
     *
     * @return {@link CommandLineHandler}
     */
    private static CommandLineHandler getCommandLineHandler()
    {
        return applicationContext.getBean(CommandLineHandler.class);
    }

}
