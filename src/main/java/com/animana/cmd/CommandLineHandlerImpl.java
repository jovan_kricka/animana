package com.animana.cmd;

import com.animana.services.io.PrintingService;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link CommandLineHandler}.
 */
@Component
public class CommandLineHandlerImpl implements CommandLineHandler
{

    private static final String NO = "no";
    private static final String NO_SHORT = "n";
    private static final String YES = "yes";
    private static final String YES_SHORT = "y";

    private static final Option HELP = Option.builder("h")
            .argName("help")
            .longOpt("help")
            .desc("Information on how to use the tool.")
            .build();
    private static final Option FILE = Option.builder("f")
            .argName("file")
            .longOpt("file")
            .hasArg()
            .desc("Path to the file to be processed.")
            .build();
    private static final Option CONTINUE_MODE = Option.builder("c")
            .argName("continue")
            .longOpt("continue")
            .desc("Automatically continue processing file after invalid line.")
            .build();

    public static final Options options = initializeCommandLineOptions();

    private final PrintingService printingService;

    @Autowired
    public CommandLineHandlerImpl(PrintingService printingService)
    {
        this.printingService = printingService;
    }

    /**
     * Initializes command line options that will be
     * used for parsing user input.
     */
    private static Options initializeCommandLineOptions()
    {
        Options options = new Options();
        options.addOption(HELP);
        options.addOption(FILE);
        options.addOption(CONTINUE_MODE);
        return options;
    }

    @Override
    public CommandLineParameters parseCommandLine(String[] args)
            throws ParseException
    {
        CommandLineParser commandLineParser = new DefaultParser();
        CommandLine commandLine = commandLineParser.parse(options, args);

        if (commandLine.hasOption(FILE.getOpt()) && commandLine.getOptionValue(FILE.getOpt()) != null)
        {
            return new CommandLineParameters(commandLine.getOptionValue(FILE.getOpt()),
                    commandLine.hasOption(CONTINUE_MODE.getOpt()));
        } else
        {
            printingService.printHelp(options);
        }
        return null;
    }

    @Override
    public boolean isValidYesNoAnswer(String answer)
    {
        return answer != null && (YES.equals(answer) || YES_SHORT.equals(answer) || NO.equals(answer) || NO_SHORT
                .equals(answer));
    }

    @Override
    public boolean isNoAnswer(String answer)
    {
        return NO.equals(answer) || NO_SHORT.equals(answer);
    }

}
