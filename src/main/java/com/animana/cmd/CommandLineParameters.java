package com.animana.cmd;

/**
 * Class holding command line parameters provided by user.
 */
public class CommandLineParameters
{

    private String filePath;
    private Boolean continueMode;

    public CommandLineParameters(String filePath, Boolean continueMode)
    {
        this.filePath = filePath;
        this.continueMode = continueMode;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public Boolean isContinueMode()
    {
        return continueMode;
    }
}
