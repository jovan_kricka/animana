package com.animana.cmd;

import org.apache.commons.cli.ParseException;

/**
 * Interface providing api for parsing {@link CommandLineParameters}.
 */
public interface CommandLineHandler
{

    /**
     * Parses command line parameters from the
     * provided command line arguments.
     *
     * @param args command line arguments
     * @return {@link CommandLineParameters}
     */
    CommandLineParameters parseCommandLine(String[] args)
            throws ParseException;

    /**
     * Checks if given string is valid 'yes'/'no' answer.
     * This method is used for command line prompts.
     *
     * @param answer string representing users answer
     * @return true if answer is valid 'yes'/'no' answer, false otherwise
     */
    boolean isValidYesNoAnswer(String answer);

    /**
     * Checks if given string is valid 'no' answer.
     * This method is used for command line prompts.
     *
     * @param answer string representing users answer
     * @return true if answer is valid 'no' answer, false otherwise
     */
    boolean isNoAnswer(String answer);

}
