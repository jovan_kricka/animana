package com.animana.services.calc;

import com.animana.exception.InvalidQuantityException;

/**
 * Interface providing the api for calculating price
 * per T-shirt given the quantity of T-shirts bought.
 */
public interface PriceCalculationService {

    /**
     * Calculates unit price of single T-shirt given the
     * total quantity of T-shirts bought by the client.
     *
     * @param quantity {@link Long} number of T-shirts bought
     * @return
     */
    Double calculatePrice(Long quantity) throws InvalidQuantityException;

}
