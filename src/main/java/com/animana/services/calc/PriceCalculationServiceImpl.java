package com.animana.services.calc;

import com.animana.exception.InvalidQuantityException;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

/**
 * Implementation of {@link PriceCalculationService}.
 */
@Service
public class PriceCalculationServiceImpl implements PriceCalculationService
{

    private LinkedList<PricePerQuantity> pricesPerQuantity = Lists.newLinkedList();

    public PriceCalculationServiceImpl()
    {
        initializePricesPerQuantity();
    }

    /**
     * Initializes list that holds a priori known prices
     * per quantities. This list will be used for interpolation.
     */
    private void initializePricesPerQuantity()
    {
        pricesPerQuantity.add(new PricePerQuantity(1l, 29d));
        pricesPerQuantity.add(new PricePerQuantity(10l, 20d));
        pricesPerQuantity.add(new PricePerQuantity(100l, 10d));
        pricesPerQuantity.add(new PricePerQuantity(500l, 8d));
        pricesPerQuantity.add(new PricePerQuantity(1000l, 7d));
        pricesPerQuantity.add(new PricePerQuantity(5000l, 5d));
        pricesPerQuantity.add(new PricePerQuantity(10_000l, 2d));
    }

    @Override
    public Double calculatePrice(Long quantity)
            throws InvalidQuantityException
    {
        if (quantity == null || quantity <= 0)
        {
            throw new InvalidQuantityException();
        }
        PricePerQuantity minimalPricePerQuantity = pricesPerQuantity.getLast();
        if (quantity >= minimalPricePerQuantity.quantity)
        {
            return minimalPricePerQuantity.price;
        }
        return interpolate(quantity);
    }

    /**
     * Determines price of T-shirt per unit using linear
     * interpolation using given quantity and predetermined
     * fixed prices per quantities.
     *
     * @param quantity number of T-shirts to calculate price for
     * @return unit price of T-shirt calculated using linear interpolation
     */
    private Double interpolate(Long quantity)
    {
        for (int i = 0; i < pricesPerQuantity.size() - 1; i++)
        {
            long leftQuantity = pricesPerQuantity.get(i).quantity;
            long rightQuantity = pricesPerQuantity.get(i + 1).quantity;
            if (quantity >= leftQuantity && quantity <= rightQuantity)
            {
                double leftPrice = pricesPerQuantity.get(i).price;
                double rightPrice = pricesPerQuantity.get(i + 1).price;
                return (leftPrice * (rightQuantity - quantity) + rightPrice * (quantity - leftQuantity)) / (rightQuantity -
                        leftQuantity);
            }
        }
        return 0d;
    }

    /**
     * Helper class that holds T-shirt price per quantity.
     */
    private static class PricePerQuantity
    {

        private Long quantity;
        private Double price;

        public PricePerQuantity(Long quantity, Double price)
        {
            this.quantity = quantity;
            this.price = price;
        }

    }

}
