package com.animana.services.io;

import org.apache.commons.cli.Options;

/**
 * Interface providing the api for displaying
 */
public interface PrintingService
{

    /**
     * Prints out T-shirt price per unit on standard output.
     *
     * @param pricePerUnit price per T-shirt
     */
    void printPricePerUnit(Double pricePerUnit);

    /**
     * Prints help for the given command line options.
     *
     * @param options {@link Options}
     */
    void printHelp(Options options);

}
