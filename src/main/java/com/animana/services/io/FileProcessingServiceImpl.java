package com.animana.services.io;

import com.animana.cmd.CommandLineHandler;
import com.animana.exception.FileProcessingAbortedException;
import com.animana.exception.InvalidQuantityException;
import com.animana.services.calc.PriceCalculationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Implementation of {@link FileProcessingService} interface.
 */
@Service
public class FileProcessingServiceImpl implements FileProcessingService
{

    private final static Logger logger = Logger.getLogger(FileProcessingServiceImpl.class);

    static final String LINE_COULD_NOT_BE_PROCESSED = "Line %d containing text %s is not a valid quantity.\n";
    private static final String SHOULD_CONTINUE_PROCESSING = "Continue processing rest of the file? yes(y)/no(n)";

    private final PrintingService printingService;
    private final PriceCalculationService priceCalculationService;
    private final CommandLineHandler commandLineHandler;

    private final BufferedReader inputReader;
    private long lineCounter;
    private boolean isContinueMode;

    @Autowired
    public FileProcessingServiceImpl(PrintingService printingService, PriceCalculationService priceCalculationService,
                                     CommandLineHandler commandLineHandler)
    {
        this.printingService = printingService;
        this.priceCalculationService = priceCalculationService;
        this.commandLineHandler = commandLineHandler;
        inputReader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void processFile(String filePath, boolean continueMode)
            throws IOException, FileProcessingAbortedException
    {
        Stream<String> stream = Files.lines(Paths.get(filePath));
        lineCounter = 0;
        this.isContinueMode = continueMode;
        stream.forEach(this::processLine);
    }

    /**
     * Processes single line of input file that represents
     * quantity of T-shirts bought by the client. Parses
     * quantity from given line, calculates price per unit
     * and prints it to standard output.
     *
     * @param line string representing line of input file
     */
    private void processLine(String line)
            throws FileProcessingAbortedException
    {
        try
        {
            lineCounter++;
            long quantity = Long.parseLong(line);
            Double pricePerUnit = priceCalculationService.calculatePrice(quantity);
            printingService.printPricePerUnit(pricePerUnit);
        } catch (NumberFormatException | InvalidQuantityException e)
        {
            logger.error(e, e.getCause());
            handleInvalidLine(line);
        }
    }

    /**
     * Prompts user given invalid line and asks if file
     * processing should be continued or aborted.
     *
     * @param line invalid line
     */
    private void handleInvalidLine(String line)
            throws FileProcessingAbortedException
    {
        System.out.printf(LINE_COULD_NOT_BE_PROCESSED, lineCounter, line);
        if (isContinueMode)
        {
            return;
        }
        System.out.println(SHOULD_CONTINUE_PROCESSING);
        String answer = "";
        while (!commandLineHandler.isValidYesNoAnswer(answer))
        {
            try
            {
                answer = inputReader.readLine();
            } catch (IOException e)
            {
                logger.error(e, e.getCause());
            }
        }
        if (commandLineHandler.isNoAnswer(answer))
        {
            throw new FileProcessingAbortedException();
        }
    }

}
