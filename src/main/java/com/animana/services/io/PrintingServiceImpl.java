package com.animana.services.io;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link PrintingService}.
 */
@Service
public class PrintingServiceImpl implements PrintingService
{
    static final String HELP_HEADER = "This is Animana tool help.";
    static final String HELP_FOOTER = "For more info contact jovankricka@gmail.com";
    static final String APP_NAME = "animana";

    @Override
    public void printPricePerUnit(Double pricePerUnit)
    {
        System.out.println(pricePerUnit);
    }

    @Override
    public void printHelp(Options options)
    {
        String header = HELP_HEADER + "\n\n";
        String footer = "\n" + HELP_FOOTER;
        String appName = APP_NAME;

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(appName, header, options, footer, true);
    }
}
