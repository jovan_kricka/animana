package com.animana.services.io;

import java.io.IOException;

/**
 * Interface providing api for processing input files
 * containing data about units purchased by clients.
 */
public interface FileProcessingService
{

    /**
     * Opens input file reads data from it line by line and processes
     * each line.
     *
     * @param filePath     {@link String} representing path to the input file
     * @param continueMode boolean that determines should we continue processing after invalid line
     * @throws IOException
     */
    void processFile(String filePath, boolean continueMode)
            throws IOException;

}
