package com.animana.services.io;

import com.animana.app.AppContext;
import com.animana.cmd.CommandLineHandlerImpl;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertThat;

/**
 * Tests for {@link PrintingServiceImpl}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class TestPrintingServiceImpl
{

    @Autowired
    private PrintingService printingService;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams()
    {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams()
    {
        System.setOut(null);
    }

    @Test
    public void shouldPrintPriceToStandardOutput()
    {
        printingService.printPricePerUnit(42d);

        assertThat(outContent.toString(), CoreMatchers.containsString("42"));
    }

    @Test
    public void shouldPrintHelpToStandardOutput()
    {
        printingService.printHelp(CommandLineHandlerImpl.options);

        assertThat(outContent.toString(), CoreMatchers.containsString(PrintingServiceImpl.APP_NAME));
        assertThat(outContent.toString(), CoreMatchers.containsString(PrintingServiceImpl.HELP_HEADER));
        assertThat(outContent.toString(), CoreMatchers.containsString(PrintingServiceImpl.HELP_FOOTER));
    }

}
