package com.animana.services.io;

import com.animana.app.AppContext;
import com.animana.exception.FileProcessingAbortedException;
import com.animana.util.ResourceUtil;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.*;

import static org.junit.Assert.assertThat;

/**
 * Tests for {@link FileProcessingServiceImpl}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class TestFileProcessingServiceImpl
{

    private static final String NON_EXISTING_FILE_PATH = "i_do_not_exist.txt";

    static
    {
        ByteArrayInputStream in = new ByteArrayInputStream("no".getBytes());
        System.setIn(in);
    }

    @Autowired
    private FileProcessingService fileProcessingService;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams()
            throws FileNotFoundException
    {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams()
    {
        System.setOut(null);
    }

    @Test(expected = IOException.class)
    public void shouldThrowIOExceptionWhenNonExistingFilePathGiven()
            throws IOException
    {
        fileProcessingService.processFile(NON_EXISTING_FILE_PATH, false);
    }

    @Test
    public void shouldPrintOutCalculatedPricesForValidQuantities()
            throws IOException
    {
        fileProcessingService.processFile(ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_VALID_FILE_PATH),
                false);

        assertThat(outContent.toString(), CoreMatchers.containsString("29.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("20.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("25.0"));
    }

    @Test
    public void shouldPrintOutCalculatedPricesForValidQuantitiesAndMessageForInvalidQuantity()
            throws IOException
    {
        fileProcessingService
                .processFile(ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_INVALID_FILE_PATH), true);

        assertThat(outContent.toString(), CoreMatchers.containsString("29.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("20.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("25.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString(ResourceUtil.INVALID_LINE_MESSAGE));
    }

    @Test(expected = FileProcessingAbortedException.class)
    public void shouldThrowFileProcessingAbortedExceptionWhenUserAbortsProcessing()
            throws IOException
    {
        fileProcessingService
                .processFile(ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_INVALID_FILE_PATH), false);
    }

}
