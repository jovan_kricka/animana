package com.animana.services.calc;

import com.animana.app.AppContext;
import com.animana.exception.InvalidQuantityException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link PriceCalculationServiceImpl}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class TestPriceCalculationServiceImpl
{

    private static final Double MIN_PRICE = 2d;
    private static final Double MID_PRICE = 25d;
    private static final Double MAX_PRICE = 29d;

    @Autowired
    private PriceCalculationService priceCalculationService;

    @Test(expected = InvalidQuantityException.class)
    public void shouldThrowInvalidQuantityExceptionWhenNegativeQuantityGiven()
            throws InvalidQuantityException
    {
        priceCalculationService.calculatePrice(-1l);
    }

    @Test(expected = InvalidQuantityException.class)
    public void shouldThrowInvalidQuantityExceptionWhenNullQuantityGiven()
            throws InvalidQuantityException
    {
        priceCalculationService.calculatePrice(null);
    }

    @Test
    public void shouldReturnMinimalPriceWhenHugeQuantityGiven()
            throws InvalidQuantityException
    {
        Double price = priceCalculationService.calculatePrice(10_000_000l);

        assertEquals(MIN_PRICE, price);
    }

    @Test
    public void shouldReturnCorrectPriceForLowerBoundQuantityGiven()
            throws InvalidQuantityException
    {
        Double price = priceCalculationService.calculatePrice(1l);

        assertEquals(MAX_PRICE, price);
    }

    @Test
    public void shouldInterpolatePriceForNonPreDefinedQuantityGiven()
            throws InvalidQuantityException
    {
        Double price = priceCalculationService.calculatePrice(5l);

        assertEquals(MID_PRICE, price);
    }

}
