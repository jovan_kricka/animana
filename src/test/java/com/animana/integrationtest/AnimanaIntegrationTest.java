package com.animana.integrationtest;

import com.animana.app.App;
import com.animana.app.AppContext;
import com.animana.util.ResourceUtil;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import static org.junit.Assert.assertThat;

/**
 * Integration test for Animana application.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class AnimanaIntegrationTest
{

    static
    {
        ByteArrayInputStream in = new ByteArrayInputStream("no".getBytes());
        System.setIn(in);
    }

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams()
            throws FileNotFoundException
    {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void cleanUpStreams()
    {
        System.setOut(null);
    }

    @Test
    public void shouldProcessValidQuantitiesCorrectly()
    {
        String[] arguments = {"-f", ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_VALID_FILE_PATH)};

        App.main(arguments);

        assertThat(outContent.toString(), CoreMatchers.containsString("29.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("20.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("25.0"));
    }

    @Test
    public void shouldProcessInValidQuantitiesCorrectly()
    {
        String[] arguments = {"-f", ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_INVALID_FILE_PATH), "-c"};

        App.main(arguments);

        assertThat(outContent.toString(), CoreMatchers.containsString("29.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("20.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("25.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString(ResourceUtil.INVALID_LINE_MESSAGE));
    }

    @Test
    public void shouldProcessInValidQuantitiesCorrectlyAndPromptUser()
    {
        String[] arguments = {"-f", ResourceUtil.getResourceFilePath(ResourceUtil.QUANTITIES_INVALID_FILE_PATH)};

        App.main(arguments);

        assertThat(outContent.toString(), CoreMatchers.containsString("29.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("20.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString("25.0"));
        assertThat(outContent.toString(), CoreMatchers.containsString(ResourceUtil.INVALID_LINE_MESSAGE));
        assertThat(outContent.toString(), CoreMatchers.containsString("File processing aborted."));
    }

}
