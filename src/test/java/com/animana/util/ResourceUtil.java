package com.animana.util;

import java.io.File;

/**
 * Util class for handling test resources.
 */
public class ResourceUtil
{

    public static final String QUANTITIES_VALID_FILE_PATH = "quantities_valid.txt";
    public static final String QUANTITIES_INVALID_FILE_PATH = "quantities_invalid.txt";
    public static final String INVALID_LINE_MESSAGE = "Line 4 containing text iamnotagoodquantity is not a valid " +
            "quantity.";

    /**
     * Gets absolute path to a file from resources folder
     * with given name.
     *
     * @param fileName name of resource file
     * @return absolute path to the file
     */
    public static String getResourceFilePath(String fileName)
    {
        ClassLoader classLoader = ResourceUtil.class.getClassLoader();
        return new File(classLoader.getResource(fileName).getFile()).getAbsolutePath();
    }

}
